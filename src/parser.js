import { getItem } from './utils/getItem';
import { updateResult } from './utils/updateResult';

const XMLParser = (data) => {
  const formattedData = data.replace(/\r\n/gm, '');
  const openedTags = [];
  const result = [];
  let i = 0;

  while(i < formattedData.length) {
    const { data, item } = getItem(formattedData, i);
    i += data.length;

    if (!data.trim()) continue;
    
    updateResult(item, result, openedTags);
  }

  return result;
};

export default XMLParser;