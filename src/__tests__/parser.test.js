import XMLParser from '../parser';

describe('XMLParser', () => {
  it('should parse an xml layout', () => {
    const xml = `
      <FileDump>
        <Message>
          <From>Joe.doe@gmail.com</From>
        </Message>
      </FileDump>`;

    const expected = [
      {
        name: 'FileDump',
        type: 'paired',
        attributes: {},
        children: [
          {
            name: 'Message',
            type: 'paired',
            attributes: {},
            children: [
              {
                name: 'From',
                type: 'paired',
                attributes: {},
                children: [
                  {
                    value: 'Joe.doe@gmail.com',
                    type: 'text'
                  }
                ]
              }
            ]
          }
        ]
      }
    ];
    const parsed = XMLParser(xml);

    expect(parsed).toEqual(expected);
  });
});