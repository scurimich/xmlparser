import { getTagData } from './getTagData';

export const getItem = (string, start) => {
  const startChar = string[start];

  if (startChar !== '<') {
    const text = parseTextItem(string, start);

    return {
      data: text,
      item: {
        value: text,
        type: 'text'
      }
    }
  }

  if (startChar === '<') {
    const tag = parseTag(string, start);
    const item = getTagData(tag);

    return {
      data: tag,
      item,
    };
  }
}

export const parseTextItem = (string, start) => {
  const nextTag = string.indexOf('<', start);
  return string.slice(start, nextTag);
};

export const parseTag = (string, start) => {
  const tagEnd = string.indexOf('>', start);
  return string.slice(start, tagEnd + 1);
};