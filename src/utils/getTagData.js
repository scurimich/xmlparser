import { separator } from '../constants';

export const getTagData = (tag) => {
  const type = getTagType(tag);
  const tagInfo = getTagInfo(tag, type);
  const name = getTagName(tagInfo);
  const attributes = getAttributes(tagInfo);

  return {
    name,
    type,
    attributes,
    children: [],
  };
};

export const getTagType = (tag) => {
  const secondChar = tag[1];
  const rightSecondChar = tag[tag.length - 2];

  if (secondChar === '?') return 'declaration';
  if (secondChar === '/') return 'paired-end';
  if (rightSecondChar === '/') return 'unpaired';
  return 'paired';
};

export const getTagInfo = (tag, type) => {
  switch(type) {
    case 'paired-end':
      return tag.slice(2, -1);
    case 'declaration':
      return tag.slice(2, -2);
    case 'unpaired':
      return tag.slice(1, -2);
    default:
      return tag.slice(1, -1);
  }
};

export const getTagName = (tagInfo) => {
  const firstSpace = tagInfo.indexOf(separator);
  return firstSpace + 1 ? tagInfo.slice(0, firstSpace) : tagInfo;
};

export const getAttributes = (tagInfo) => {
  const firstSpace = tagInfo.indexOf(separator);
  if (!(firstSpace + 1)) return {};

  const rawAttributes = tagInfo.split(separator).slice(1);

  return rawAttributes.reduce((res, cur) => {
    if (!cur) return res;
      const [ attrName, attrValue ] = cur.split('=');

      res[attrName] = attrValue ? attrValue.slice(1, -1) : true;
      return res;
  }, {});
};