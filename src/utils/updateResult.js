export const updateResult = (item, result, openedTags) => {
  const { name, type } = item;

    switch(type) {
      case 'paired': {
        return processPaired(item, result, openedTags);
      }
      case 'paired-end': {
        return processPairedEnd(name, openedTags);
      }
      default: {
        return processItem(item, result, openedTags)
      }
    }
};

export const processPaired = (item, result, openedTags) => {
  const lastOpenedTag = openedTags[openedTags.length - 1];

  if (lastOpenedTag) {
    lastOpenedTag.children = [...lastOpenedTag.children, item];
    openedTags.push(item);
    return;
  }
  
  result.push(item);
  openedTags.push(item);
};

export const processPairedEnd = (name, openedTags) => {
  const lastOpenedTag = openedTags[openedTags.length - 1];

  if (!lastOpenedTag) return;

  const pairedIndex = openedTags.reduceRight((res, cur, ndx) => {
    if (cur.name === name) return ndx;
    return res;
  }, -1);
  if (pairedIndex + 1) openedTags.splice(pairedIndex, 1);
};

export const processItem = (item, result, openedTags) => {
  const lastOpenedTag = openedTags[openedTags.length - 1];

  if (lastOpenedTag) {
    lastOpenedTag.children = [...lastOpenedTag.children, item];
    return;
  }

  result.push(item);
};