import {
  updateResult,
  processPaired,
  processPairedEnd,
  processItem
} from '../updateResult';

describe('updateResult', () => {
  it('should update result with new item', () => {
    const item = {
      name: 'xml',
      type: 'declaration',
      attributes: {
        version: '1.0',
        encoding: 'UTF-8',
        standalone: 'no'
      },
      children: []
    };
    const result = [];
    const openedTags = [];

    const expectedResult = [item];

    updateResult(item, result, openedTags);

    expect(result).toEqual(expectedResult);
  });
});

describe('processPaired', () => {
  it('should add an item to last opened tag', () => {
    const item = {
      name: 'Message',
      type: 'paired',
      attributes: {},
      children: []
    };
    const parent = {
      name: 'FileDump',
      type: 'paired',
      attributes: {},
      children: []
    };
    const openedTags = [parent];
    const result = [parent];

    const expectedResult = [
      { ...parent, children: [item] }
    ];
    const expectedOpenedTags = [ ...openedTags, item];

    processPaired(item, result, openedTags);

    expect(openedTags).toEqual(expectedOpenedTags);
    expect(result).toEqual(expectedResult);
  });

  it('should add an item to result', () => {
    const item = {
      name: 'FileDump',
      type: 'paired',
      attributes: {},
      children: []
    };
    const openedTags = [];
    const result = [];

    const expectedResult = [item];
    const expectedOpenedTags = [item];

    processPaired(item, result, openedTags);

    expect(openedTags).toEqual(expectedOpenedTags);
    expect(result).toEqual(expectedResult);
  });
});

describe('processPairedEnd', () => {
  it('should remove a paired tag from openedTags', () => {
    const item = 'Message';
    const paired = {
      name: 'Message',
      type: 'paired',
      attributes: {},
      children: []
    };
    const openedTags = [paired];

    const expectedOpenedTags = [];

    processPairedEnd(item, openedTags);

    expect(openedTags).toEqual(expectedOpenedTags);
  });

  it(`shouldm't do anything if tag wasn't opened`, () => {
    const item = 'Message';
    const paired = {
      name: 'FileDump',
      type: 'paired',
      attributes: {},
      children: []
    };
    const openedTags = [paired];

    processPairedEnd(item, openedTags);

    expect(openedTags).toEqual(openedTags);
  });
});

describe('processItem', () => {
  it('should add item to result', () => {
    const item = {
      name: 'xml',
      type: 'declaration',
      attributes: {
        version: '1.0',
        encoding: 'UTF-8',
        standalone: 'no'
      },
      children: []
    };
    const result = [];
    const openedTags = [];

    const expectedResult = [item];

    processItem(item, result, openedTags);

    expect(result).toEqual(expectedResult);
  });

  it('should add item to last opened tag', () => {
    const item = {
      data: 'Joe.doe@gmail.com',
      item: {
        value: 'Joe.doe@gmail.com',
        type: 'text'
      }
    };
    const parent = {
      name: 'FileDump',
      type: 'paired',
      attributes: {},
      children: []
    };
    const result = [parent];
    const openedTags = [parent];

    const expectedResult = [
      { ...parent, children: [item] }
    ]

    processItem(item, result, openedTags);

    expect(result).toEqual(expectedResult);
  });
});