import { getItem, parseTextItem, parseTag } from '../getItem';

jest.mock('../getTagData', () => ({
  getTagData: () => ({})
}));

const string = '<FileDump><Message>    <From>Joe.doe@gmail.com</From>';

describe('getItem', () => {
  it('should return a tag from a string', () => {
    const start = 0;
    const expected = {
      data: '<FileDump>',
      item: {}
    };

    const result = getItem(string, start);

    expect(result).toEqual(expected);
  });

  it('should return a text from a string', () => {
    const start = 29;
    const expected = {
      data: 'Joe.doe@gmail.com',
      item: {
        value: 'Joe.doe@gmail.com',
        type: 'text'
      }
    };

    const result = getItem(string, start);

    expect(result).toEqual(expected);
  });
});

describe('parseTextItem', () => {
  it('should return a text from a string', () => {
    const start = 29;
    const expected = 'Joe.doe@gmail.com';

    const result = parseTextItem(string, start);
    
    expect(result).toEqual(expected);
  });
});

describe('parseTag', () => {
  it('should return a tag from a string', () => {
    const start = 10;
    const expected = '<Message>';

    const result = parseTag(string, start);
    
    expect(result).toEqual(expected);
  });
});