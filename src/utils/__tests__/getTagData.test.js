import {
  getTagData,
  getTagType,
  getTagInfo,
  getTagName,
  getAttributes
} from '../getTagData';

describe('getTagData', () => {
  it('should return all the data from a declaration tag', () => {
    const tag = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
    const expected = {
      name: 'xml',
      type: 'declaration',
      attributes: {
        version: '1.0',
        encoding: 'UTF-8',
        standalone: 'no'
      },
      children: []
    };
    const result = getTagData(tag);

    expect(result).toEqual(expected);
  });
  
  it('should return all the data from a paired tag', () => {
    const tag = '<FileDump>';
    const expected = {
      name: 'FileDump',
      type: 'paired',
      attributes: {},
      children: []
    };
    const result = getTagData(tag);

    expect(result).toEqual(expected);
  });
  
  it('should return all the data from a unpaired tag', () => {
    const tag = '<FileDump/>';
    const expected = {
      name: 'FileDump',
      type: 'unpaired',
      attributes: {},
      children: []
    };
    const result = getTagData(tag);

    expect(result).toEqual(expected);
  });
  
  it('should return all the data from a paired-end tag', () => {
    const tag = '</FileDump>';
    const expected = {
      name: 'FileDump',
      type: 'paired-end',
      attributes: {},
      children: []
    };
    const result = getTagData(tag);

    expect(result).toEqual(expected);
  });
});

describe('getTagType', () => {
  it('should return type of a declaration tag', () => {
    const tag = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
    const result = getTagType(tag);

    expect(result).toBe('declaration');
  });

  it('should return type of a paired tag', () => {
    const tag = '<Message>';
    const result = getTagType(tag);

    expect(result).toBe('paired');
  });
});

describe('getTagInfo', () => {
  it('should return a declaration tag without brackets', () => {
    const tag = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
    const type = 'declaration';
    const result = getTagInfo(tag, type);

    expect(result).toBe('xml version="1.0" encoding="UTF-8" standalone="no"');
  });

  it('should return an unpaired tag without brackets', () => {
    const tag = '<Message/>';
    const type = 'unpaired';
    const result = getTagInfo(tag, type);

    expect(result).toBe('Message');
  });
});

describe('getTagName', () => {
  it('should return the name of a tag with attributes', () => {
    const tagInfo = 'xml version="1.0" encoding="UTF-8" standalone="no"';
    const result = getTagName(tagInfo);

    expect(result).toBe('xml');
  });

  it('should return the name of a tag without attributes', () => {
    const tagInfo = 'xml';
    const result = getTagName(tagInfo);

    expect(result).toBe('xml');
  });
});

describe('getAttributes', () => {
  it('should return attributes of a tag', () => {
    const tagInfo = 'xml version="1.0" encoding="UTF-8" standalone="no"';
    const expected = {
      version: '1.0',
      encoding: 'UTF-8',
      standalone: 'no'
    };

    const result = getAttributes(tagInfo);

    expect(result).toEqual(expected);
  });

  it('should return attributes of a tag with different attributes', () => {
    const tagInfo = 'xml version="1.0" encoding="UTF-8" standalone="no" testAttribute';
    const expected = {
      version: '1.0',
      encoding: 'UTF-8',
      standalone: 'no',
      testAttribute: true
    };

    const result = getAttributes(tagInfo);

    expect(result).toEqual(expected);
  });

  it('should return an empty object fot a tag without attributes', () => {
    const tagInfo = 'Message';
    const expected = {};

    const result = getAttributes(tagInfo);

    expect(result).toEqual(expected);
  });
});
