import fs from 'fs';
import XMLParser from './src/parser.js';

function processData(fileName, callback) {
  const readStream = fs.createReadStream(fileName);
  
  readStream.on('data', chunk => {
    const xmlChunk = chunk.toString();
    const parsedJSON = XMLParser(xmlChunk);

    callback(JSON.stringify(parsedJSON));
  });

  readStream.on('error', err => {
    console.log(err);
  });
}

processData('./data/CodeTest-XML.xml', db.save);